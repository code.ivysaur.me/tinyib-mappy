# tinyib-mappy

![](https://img.shields.io/badge/written%20in-PHP-blue)

A more respectable take on the common imageboard.

Tinyib-mappy is a simple imageboard in PHP, supporting most of the basic features one would expect. It deviates slightly from the standard Futaba mannerisms: deleting one's own posts is handled by IP+timeout rather than a password, Noko is by default, and Sage is by checkbox.

This project is based on changes to the GPLv3 TinyIB (forked from the 1.2 release), from which it inherits the name and license. I have been slowly removing, rearranging and adding things to suit my needs while still keeping the board simple and lightweight.

This project also includes a script to import posts from magmagateau's now-defunct Fuukaba Basic imageboard, and a script to update posts from older versions of TinyIB.

Features:
- Captcha
- RSS
- Locking threads
- New, unobstrusive, professional style
- All DB queries precompiled against SQL injection
- Tidier file structure
- More configurable options

Usage:
- Needs php5-pdo-sqlite. Recommend PHP 5 and no magic_quotes or other weird ini settings.

This project is based on TinyIB and hence retains the GPLv3 license.

Original project page: https://code.google.com/p/tinyib-mappy/


## Download

- [⬇️ tinyib-mappy-boardupdate-r1.7z](dist-archive/tinyib-mappy-boardupdate-r1.7z) *(568B)*
- [⬇️ tinyib-mappy-boardcnv-r1.rar](dist-archive/tinyib-mappy-boardcnv-r1.rar) *(3.75 KiB)*
- [⬇️ tinyib-mappy-20120311.7z](dist-archive/tinyib-mappy-20120311.7z) *(13.87 KiB)*
- [⬇️ tinyib-mappy-20111027.7z](dist-archive/tinyib-mappy-20111027.7z) *(13.86 KiB)*
- [⬇️ tinyib-mappy-20111020.7z](dist-archive/tinyib-mappy-20111020.7z) *(13.88 KiB)*
- [⬇️ tinyib-mappy-20110610.7z](dist-archive/tinyib-mappy-20110610.7z) *(13.65 KiB)*
- [⬇️ tinyib-mappy-20110507.rar](dist-archive/tinyib-mappy-20110507.rar) *(11.99 KiB)*
